module.exports = {
    PUBLIC: 1,
    TIMEOUT: 15000,
    MAX_INSTANCES: 1,
    URL: '/run',
    DB: 'edgar',
    poolConfig: {
        user: 'postgres',
        database: 'edgar',
        password: 'ChangeThisPassword!postgres',
        port: 5432,
        host: 'edgar_postgres',
        max: 100,
        idleTimeoutMillis: 30000,
        connectionTimeoutMillis: 2000,
    }
};
