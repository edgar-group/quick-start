SET DOCKER_COMPOSE=docker-compose

IF NOT EXIST %DOCKER_COMPOSE% (
   SET DOCKER_COMPOSE=docker compose
)

REM Stop and remove all Docker containers, networks, volumes, and images associated with the docker-compose.yml file
%DOCKER_COMPOSE% down -v --remove-orphans

REM Bring up the containers in detached mode (-d) and pull the latest images for all services (--pull always), and remove any orphaned containers
%DOCKER_COMPOSE% up -d --pull always --remove-orphans edgar_postgres edgar_pg_runner_postgres

REM Sleep for 10 seconds
TIMEOUT /T 10

REM Bring up the containers in detached mode (-d) again to ensure that they are started
%DOCKER_COMPOSE% up -d --remove-orphans